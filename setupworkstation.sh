
sudo yum install wget -y
sudo yum install ntp -y
sudo yum install python -y
sudo yum install python-boto -y
sudo yum install unzip -y
sudo yum install git -y
sudo yum install ansible -y
sudo ntpdate time.apple.com 

wget http://download.fedoraproject.org/pub/epel/6/x86_64/epel-release-6-8.noarch.rpm
sudo yum install epel-release-6-8.noarch.rpm -y

wget https://s3.amazonaws.com/aws-cli/awscli-bundle.zip
unzip awscli-bundle.zip
sudo ./awscli-bundle/install -i /usr/local/aws -b /usr/local/bin/aws

sudo yum update -y

sudo rm epel-release-6-8.noarch.rpm -rf
sudo rm awscli-bundle.zip -rf

sudo git clone git://github.com/cotdsa/cumulus.git
cd cumulus
sudo python setup.py install
cd ..

sudo rm –f .boto

read -p  'Type your AWS Access Key, followed by [ENTER]:' access
read -p  'Type your AWS Secret Key, followed by [ENTER]:' secret

cat <<EOF >> .boto
[Credentials]
aws_access_key_id = $access
aws_secret_access_key = $secret
EOF

read -p 'Type your Bitbucket username, followed by [ENTER]:' user
read -p 'Type your Bitbucket password, followed by [ENTER]:' pass
sudo git clone https://$user:$pass@bitbucket.org/odecee/nab-mlc-idc.git
cd nab-mlc-idc/cloudformation

echo "Edit the config file : cumulus_vpc_and_app  and  follow other steps in cumulus_vpc_and_app.readme"
echo “To create an environment, use : cumulus -y cumulus_vpc_and_app.yaml -a create "
echo “To create an AMI, use : TBC "